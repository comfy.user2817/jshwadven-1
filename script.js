/* Теоритичні питання:
1. Поясніть своїми словами, як ви розумієте, як працює прототипне наслідування в Javascript
    дозволяє об’єктам успадковувати властивості та методи від інших об’єктів
2. Для чого потрібно викликати super() у конструкторі класу-нащадка?
    для переприсвоєння новому конструктору класу значень з конструктора классу який є прототипом першого 
*/ 


//1

class Employee {
    constructor(name, age, salary) {
        this.name = name
        this.age = age
        this.salary = salary
    }
    get itIsName(){
        return this.name
    }
    set itIsName(name){
        this.name = name
    }
    get itIsAge() {
        return this.age
    }
    set itIsAge(age) {
        this.age = age
    }
    get itIsSalary() {
        return this.salary
    }
    set itIsSalary(salary) {
        this.salary = salary
    }
}


class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary)
        this.lang = lang
    }
    get itIsSalary() {
        return this.salary*3 
    }
}


let Jon = new Programmer("John", 30, 50000, "JavaScript");

Jon.itIsSalary







